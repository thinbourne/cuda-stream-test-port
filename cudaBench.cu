/* cudaBench.cu -- CUDA port of STREAM benchmark
 * (c) - Janakarajan Natarajan
 *
 * Operations -- Copy:     A[] = B[]
 *               Scale:    A[] = B[] * s
 *               Add:      A[] = B[] + C[]
 *               TriAdd:   A[] = B[] + C[] * s
 *
 */
#include <stdio.h>
#include <inttypes.h>

int *h_a, *h_b, *h_f, *d_a, *d_b, *d_f;
int size = 8;

/* Function: STREAM Copy port for CUDA
 * 
 * Param: Array A - a
 *        Final   - Copy buffer
 *        Size    - Size of the array and final
 *
 */
__global__ void GPUCopy(int *a, int *final, int size){
	int id = blockIdx.x * blockDim.x + threadIdx.x;

	if(id < size)
		final[id] = a[id];
}

/* Function: STREAM Scale port for CUDA
 *
 * Param: Array A - a
 *        Final - Scale output buffer
 *        Size  - Size of the array and final
 *        Scale - Scalar to be multiplied with a
 *
 */
__global__ void GPUScale(int *a, int *final, int size, int scale){
	int id = blockIdx.x * blockDim.x + threadIdx.x;

	if(id < size)
		final[id] = a[id] * scale;
}

/* Function: STREAM Add port for CUDA
 *
 * Param: Array A - a
 *        Array B - b
 *        Final   - Scale output buffer
 *        Size    - Size of the array and final
 *
 */
__global__ void GPUAdd(int *a, int *b, int *final, int size){
	int id = blockIdx.x * blockDim.x + threadIdx.x;

	if(id < size)
		final[id] = a[id] + b[id];
}

/* Function: STREAM TriAdd port for CUDA
 *
 * Param: Array A - a
 *        Array B - b
 *        Final   - Scale output buffer
 *        Size    - Size of the array and final
 *        Scale   - Scalar to be multiplied with a
 *
 */
__global__ void GPUTriAdd(int *a, int *b, int *final, int size, int scale){
	int id = blockIdx.x * blockDim.x + threadIdx.x;

	if(id < size)
		final[id] = a[id] + (b[id] * scale);
}

/* Function: Read time stamp counter
 * 
 * Returns: 64-bit tsc
 *
 */
uint64_t rdtsc(){
	unsigned int start, end;
	asm volatile("rdtsc"
					: "=a" (start), "=d" (end));
	return (uint64_t)end << 32 | start;
}

/* Function: Initialize cpu and gpu memory
 *
 * Returns: -1 - Error
 *           0 - Success
 *
 */
int gpu_init_mem(){
	int i;
	h_a = (int*)malloc(sizeof(int) * size);
	if(!h_a){
		printf("Array A mem allocate error\n");
		return -1;
	}
	h_b = (int*)malloc(sizeof(int) * size);
	if(!h_b){
		printf("Array B mem allocate error\n");
		return -1;
	}
	h_f = (int*)malloc(sizeof(int) * size);
	if(!h_f){
		printf("Array B mem allocate error\n");
		return -1;
	}
	for(i = 0; i < size; i++){
		h_a[i] = i;
		h_b[i] = i;
		h_f[i] = -1;
	}	
	
	/* Initialize memory in device --> GPU */
	if(cudaMalloc(&d_a, sizeof(int) * size) != cudaSuccess){
		printf("Cuda Array A malloc error\n");
		free(h_a);
		free(h_b);
		return -1;
	}
	if(cudaMalloc(&d_b, sizeof(int) * size) != cudaSuccess){
		printf("Cuda Array B malloc error\n");
		free(h_a);
		free(h_b);
		cudaFree(d_a);
		return -1;
	}
	if(cudaMalloc(&d_f, sizeof(int) * size) != cudaSuccess){
		printf("Cuda Array F malloc error\n");
		free(h_a);
		free(h_b);
		free(h_f);
		cudaFree(d_a);
		cudaFree(d_b);
		return -1;
	}
	
	/* Copy memory from host to device Host --> Device */
	if(cudaMemcpy(d_a, h_a, sizeof(int) * size, cudaMemcpyHostToDevice) 
															!= cudaSuccess){
		printf("Cuda Memcpy A error\n");
		free(h_a);
		free(h_b);
		free(h_f);
		cudaFree(d_a);
		cudaFree(d_b);
		cudaFree(d_f);
		return -1;
	}
	if(cudaMemcpy(d_b, h_b, sizeof(int) * size, cudaMemcpyHostToDevice) 
															!= cudaSuccess){
		printf("Cuda Memcpy B error\n");
		free(h_a);
		free(h_b);
		free(h_f);
		cudaFree(d_a);
		cudaFree(d_b);
		cudaFree(d_f);
		return -1;
	}
	if(cudaMemcpy(d_f, h_f, sizeof(int) * size, cudaMemcpyHostToDevice) 
															!= cudaSuccess){
		printf("Cuda Memcpy F error\n");
		free(h_a);
		free(h_b);
		free(h_f);
		cudaFree(d_a);
		cudaFree(d_b);
		cudaFree(d_f);
		return -1;
	}
	return 0;
}

/* Function: Free CPU and GPU memory */
void free_mem(){
	free(h_a);
	free(h_b);
	free(h_f);
	cudaFree(d_a);
    cudaFree(d_b);
	cudaFree(d_f);
}
	
int main(int argc, char **argv)
{
	int j, count, ret, scale = 2, threads;

	uint64_t copy_start, copy_end;
	uint64_t scale_start, scale_end;
	uint64_t add_start, add_end;
	uint64_t triadd_start, triadd_end;

	if(argc < 2){
		printf("Enter count\n");
		return 0;
	}
	
	/* Number of iterations to repeat benchmark*/
	/* Given as input by user */
	count = atoi(argv[1]);
	printf("SIZE\tCOPY\tSCALE\tADD\tTRIADD\n");
	for(j = 0; j < count; j++){
		ret = gpu_init_mem();
		if(ret != 0){
			return -1;	
		}

		/* Number of threads = size if < 512 */
		/* Else threads = 512 */
		threads = (size > 512? 512: size);	

		copy_start = rdtsc();
		GPUCopy<<<size/512 + 1, threads>>>(d_a, d_f, size);
		cudaDeviceSynchronize();
		copy_end = rdtsc();

		/* Copy computation from device to host GPU --> CPU */
		if(cudaMemcpy(h_f, d_f, sizeof(int) * size, cudaMemcpyDeviceToHost)
															!= cudaSuccess){
			printf("Cuda Memcpy D->H->F error\n");
			return -1;
		}

		ret = gpu_init_mem();
		if(ret != 0){
			return -1;	
		}

		/* Add event to the stream */
		scale_start = rdtsc();
		GPUScale<<<size/512 + 1, threads>>>(d_a, d_f, size, scale);
		cudaDeviceSynchronize();
		scale_end = rdtsc();

		/* Copy computation from device to host GPU --> CPU */
		if(cudaMemcpy(h_f, d_f, sizeof(int) * size, cudaMemcpyDeviceToHost)
															!= cudaSuccess){
			printf("Cuda Memcpy D->H->F error\n");
			free_mem();
			return -1;
		}

		ret = gpu_init_mem();
		if(ret != 0){
			return -1;	
		}

		add_start = rdtsc();
		GPUAdd<<<size/512 + 1, threads>>>(d_a, d_b, d_f, size);
		cudaDeviceSynchronize();
		add_end = rdtsc();

		/* Copy computation from device to host GPU --> CPU */
		if(cudaMemcpy(h_f, d_f, sizeof(int) * size, cudaMemcpyDeviceToHost)
															!= cudaSuccess){
			printf("Cuda Memcpy D->H->F error\n");
			free_mem();
			return -1;
		}

		ret = gpu_init_mem();
		if(ret != 0){
			return -1;	
		}

		triadd_start = rdtsc();
		GPUTriAdd<<<size/512 + 1, threads>>>(d_a, d_b, d_f, size, scale);
		cudaDeviceSynchronize();
		triadd_end = rdtsc();

		/* Copy computation from device to host GPU --> CPU */
		if(cudaMemcpy(h_f, d_f, sizeof(int) * size, cudaMemcpyDeviceToHost)
															!= cudaSuccess){
			printf("Cuda Memcpy D->H->F error\n");
			free_mem();
			return -1;
		}

		printf("%d\t%d\t%d\t%d\t%d\n", size, (int)(copy_end - copy_start)/size,
									(int)(scale_end - scale_start)/size,
									(int)(add_end - add_start)/size,
									(int)(triadd_end - triadd_start)/size);
		free_mem();
		size *= 2;
	}
	return 0;	
}
